#!/bin/bash

# Calculate how many files are in the folder.

kismet_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c .kismet)
pcapng_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c .pcapng)
pcapppi_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c .pcapppi)
#airodump_files=$(ls ~/Desktop/DSRT/Airodump/ | grep -c .csv)
probequest_files=$(ls ~/Desktop/DSRT/Probequest/ | grep -c .csv)

export KISMETFILES=$kismet_files

# Calculate how many files need to be renamed.

new_kismet_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c kismet.kismet)
new_pcapng_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c kismet.pcapng)
new_pcapppi_files=$(ls ~/Desktop/DSRT/Kismet/ | grep -c kismet.pcapppi)
#new_airodump_files=$(ls ~/Desktop/DSRT/Airodump/ | grep -c airodump-01.csv)
new_probequest_files=$(ls ~/Desktop/DSRT/Probequest/ | grep -c probes.csv)
# Moving the files if needed

# Kismet
cd ~/Desktop/DSRT/Kismet/
if [[ $kismet_files != 0 ]]; then
    if [[ $new_kismet_files != 0 ]]; then
        # csv
        kismet_log_to_csv --in kismet.kismet --out $KISMETFILES'.csv' 
        #kml
        kismetdb_to_kml --in kismet.kismet --out $KISMETFILES'.kml' 2> /dev/null
        #wigle csv
        kismetdb_to_wiglecsv --in kismet.kismet --out $KISMETFILES'wigle.csv' 2> /dev/null
        sed -i '1d' *wigle.csv 2> /dev/null        
        mv kismet.kismet $kismet_files'.kismet'
        mv $KISMETFILES'.csv' ./csv
        mv $KISMETFILES'wigle.csv' ./csv
        mv $KISMETFILES'.kml' ./kml
    fi
fi

# PCAPNG
cd ~/Desktop/DSRT/Kismet/ 
if [[ $pcapng_files != 0 ]]; then
    if [[ $new_pcapng_files != 0 ]]; then
        mv kismet.pcapng $pcapng_files'.pcapng'
    fi
fi

#PCAPPPI
cd ~/Desktop/DSRT/Kismet/ 
if [[ $pcapppi_files != 0 ]]; then                                         
    if [[ $new_pcapppi_files != 0 ]]; then                                 
        mv kismet.pcapppi $pcapppi_files'.pcapppi'                                 
    fi                                                                  
fi

 # Airodump
#cd ~/Desktop/DSRT/Airodump/
#if [[ $airodump_files != 0 ]]; then
#    if [[ $new_airodump_files != 0 ]]; then
#        mv airodump-01.csv $airodumpfiles'.csv'
#    fi
#fi

# Probequest
cd ~/Desktop/DSRT/Probequest/
if [[ $probequest_files != 0 ]]; then
    if [[ $new_probequest_files != 0 ]]; then
        mv probes.csv 'probe'$probequest_files'.csv'
    fi
fi

# Remove the kismet.journal file
rm ~/Desktop/DSRT/Kismet/*.kismet-journal 2> /dev/null

# Converting .kismet files into something more useful
cd ~/Desktop/DSRT/Kismet/
#if [[ $new_kismet_files != 0 ]]; then
if [[ 2 == 1 ]]; then
fi
# Removing the header column from the wigle csv

cd ~/Desktop/DSRT/Kismet/
if [[ $new_kismet_files != 0 ]]; then
fi

