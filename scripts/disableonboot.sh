#!/bin/bash
#
#Disables all DTM services and Enables NetworkManager

systemctl disable kismet
systemctl disable probequest
systemctl disable airodump
systemctl disable gpsd
systemctl enable NetworkManager

zenity --info --text "The DSRT's Digital Terrain Mapping services will not start on boot."

